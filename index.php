<html lang="pl">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="main.css">
    </head>
    <body>
        <header>
            <div class="content">
                <div id="logo">logo</div>
                <nav class="nav">
                    <ul class="top-menu">
                        <li><a href="#">link</a> </li>
                        <li><a href="#">link</a> </li>
                        <li><a href="#">link</a> </li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="content">
            <?php include"left-sidebar.php"?>
            <?php include"main-column.php"?>
            <?php include"right-sidebar.php"?>
        </div>

        <footer>
            <div class="content">
                <p>&copy;2016 Copyright</p>
            </div>
        </footer>
    </body>
</html>